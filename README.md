# All Different Directions

See: https://open.kattis.com/problems/alldifferentdirections

Usage:

```
composer install
php index.php
```

or you can just run the all-in-one file:

```
php index_for_kattis.php
```