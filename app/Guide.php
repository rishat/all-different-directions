<?php

namespace App;

class Guide
{
    /** @var string */
    protected $input = '';

    /** @var Point */
    protected $point = null;

    protected function calc()
    {
        $array = explode(' ', $this->input);
        if (count($array) < 2) {
            throw new \InvalidArgumentException('Invalid input format');
        }
        $this->point = new Point($array[0], $array[1]);
        $direction = 0;
        foreach ($array as $key => $word) {
            if ($key > 1) {
                if ($word === 'start') {
                    $direction = (float)$array[$key + 1];
                } elseif ($word === 'turn') {
                    $direction += (float)$array[$key + 1];
                } elseif ($word === 'walk') {
                    $this->point->move($direction, (float)$array[$key + 1]);
                }
            }
        }
    }

    public function getDestinationPoint()
    {
        return $this->point;
    }

    public function __construct($input)
    {
        $this->input = $input;
        $this->calc();
    }
}