<?php

require_once(__DIR__ . '/vendor/autoload.php');

use App\Point;
use App\Guide;

$resultMessage = '';

while (true) {
    $numberOfGuides = (int)trim(fgets(STDIN));
    if ($numberOfGuides == 0) {
        break;
    }

    $guides = [];
    for ($i = 0; $i < $numberOfGuides; $i++) {
        $input = trim(fgets(STDIN));
        $guides[] = new Guide($input);
    }

    $pointsArray = [];
    foreach ($guides as $guide) {
        $pointsArray[] = $guide->getDestinationPoint();
    }

    $averagePoint = Point::average($pointsArray);
    $maxDistance = Point::maxDistance($pointsArray, $averagePoint);

    $resultMessage .=
        $averagePoint->getX()
        . ' ' . $averagePoint->getY()
        . ' ' . $maxDistance
        . PHP_EOL;
}

echo $resultMessage;
