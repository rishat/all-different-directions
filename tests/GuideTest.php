<?php

namespace Tests;

use App\Guide;
use PHPUnit\Framework\TestCase;

class GuideTest extends TestCase
{
    public function guideDataProvider()
    {
        return [
            ['0 0 start 0 walk 0', [0, 0]],
            ['1 1 start 0 walk 0', [1, 1]],
            ['1 1 start 90 walk 2', [1, 3]],
            ['0 0 start -45 walk 1.4142', [1, -1]],
        ];
    }

    /**
     * @dataProvider guideDataProvider
     * @param $input
     * @param $expected
     */
    public function testGuide($input, $expected)
    {
        $guide = new Guide($input);
        $this->assertEquals($expected, $guide->getDestinationPoint()->getCoordinates(), '', 0.0001);
    }
}