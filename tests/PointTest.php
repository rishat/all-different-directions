<?php

namespace Tests;

use App\Point;
use PHPUnit\Framework\TestCase;

class PointTest extends TestCase
{
    public function moveDataProvider()
    {
        return [
            [0, 0, 0, 100, [100, 0]],
            [0, 0, 90, 100, [0, 100]],
            [100, 100, 0, 100, [200, 100]],
            [0, 0, 45, 100, [70.7106, 70.7106]]
        ];
    }

    /**
     * @dataProvider moveDataProvider
     * @param $x
     * @param $y
     * @param $degrees
     * @param $distance
     * @param $expected
     */
    public function testMove($x, $y, $degrees, $distance, $expected)
    {
        $point = new Point($x, $y);
        $point->move($degrees, $distance);
        $this->assertEquals($expected, $point->getCoordinates(), '', 0.0001);
    }

    public function averageDataProvider()
    {
        return [
            [[[1, 1], [2, 2]], [1.5, 1.5]],
            [[[1, 1], [2, 2], [3, 3]], [2, 2]],
            [[[1, 1], [-1, -1]], [0, 0]],
        ];
    }

    /**
     * @dataProvider averageDataProvider
     * @param $coordinates
     * @param $expected
     */
    public function testAverage($coordinates, $expected)
    {
        $pointArray = [];
        foreach ($coordinates as $coordinate) {
            $pointArray[] = new Point($coordinate[0], $coordinate[1]);
        }
        $point = Point::average($pointArray);
        $this->assertEquals($expected, $point->getCoordinates(), '', 0.0001);
    }

    public function distanceDataProvider()
    {
        return [
            [[0, 0], [1, 1], 1.4142],
            [[100, 0], [-100, 0], 200],
        ];
    }

    /**
     * @dataProvider distanceDataProvider
     * @param $coordinate1
     * @param $coordinate2
     * @param $expected
     */
    public function testDistance($coordinate1, $coordinate2, $expected)
    {
        $point1 = new Point($coordinate1[0], $coordinate1[1]);
        $point2 = new Point($coordinate2[0], $coordinate2[1]);
        $this->assertEquals($expected, Point::distance($point1, $point2), '', 0.0001);
    }
}