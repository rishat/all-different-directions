<?php

/*
 * All in one file for Kattis
 */

class Guide
{
    /** @var string */
    protected $input = '';

    /** @var Point */
    protected $point = null;

    protected function calc()
    {
        $array = explode(' ', $this->input);
        if (count($array) < 2) {
            throw new \InvalidArgumentException('Invalid input format');
        }
        $this->point = new Point($array[0], $array[1]);
        $direction = 0;
        foreach ($array as $key => $word) {
            if ($key > 1) {
                if ($word === 'start') {
                    $direction = (float)$array[$key + 1];
                } elseif ($word === 'turn') {
                    $direction += (float)$array[$key + 1];
                } elseif ($word === 'walk') {
                    $this->point->move($direction, (float)$array[$key + 1]);
                }
            }
        }
    }

    public function getDestinationPoint()
    {
        return $this->point;
    }

    public function __construct($input)
    {
        $this->input = $input;
        $this->calc();
    }
}

class Point
{
    public $x;
    public $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Move point
     * The $degrees parameter is in degrees, not in radians
     *
     * @param $degrees
     * @param $distance
     */
    public function move($degrees, $distance)
    {
        $this->x += cos(deg2rad($degrees)) * $distance;
        $this->y += sin(deg2rad($degrees)) * $distance;
    }

    public function getCoordinates()
    {
        return [$this->x, $this->y];
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }

    /**
     * Calculate the average Point
     *
     * @param Point[] $pointsArray
     * @return Point
     */
    public static function average($pointsArray): Point
    {
        $x = 0;
        $y = 0;
        foreach ($pointsArray as $point) {
            $x += $point->getX();
            $y += $point->getY();
        }
        return new Point($x / count($pointsArray), $y / count($pointsArray));
    }

    /**
     * Find the max distance form Point Array to Point
     *
     * @param Point[] $pointsArray
     * @param Point $pointToCompare
     * @return float
     */
    public static function maxDistance($pointsArray, $pointToCompare): float
    {
        $maxDistance = 0;
        foreach ($pointsArray as $point) {
            $maxDistance = max(
                $maxDistance,
                Point::distance($pointToCompare, $point)
            );
        }
        return $maxDistance;
    }

    /**
     * Calculate the distance between 2 Points
     *
     * @param Point $point1
     * @param Point $point2
     * @return float
     */
    public static function distance($point1, $point2): float
    {
        return sqrt(
            (($point1->getX() - $point2->getX()) ** 2) + (($point1->getY() - $point2->getY()) ** 2)
        );
    }
}

$resultMessage = '';

while (true) {
    $numberOfGuides = (int)trim(fgets(STDIN));
    if ($numberOfGuides == 0) {
        break;
    }

    $guides = [];
    for ($i = 0; $i < $numberOfGuides; $i++) {
        $input = trim(fgets(STDIN));
        $guides[] = new Guide($input);
    }

    $pointsArray = [];
    foreach ($guides as $guide) {
        $pointsArray[] = $guide->getDestinationPoint();
    }

    $averagePoint = Point::average($pointsArray);
    $maxDistance = Point::maxDistance($pointsArray, $averagePoint);

    $resultMessage .=
        $averagePoint->getX()
        . ' ' . $averagePoint->getY()
        . ' ' . $maxDistance
        . PHP_EOL;
}

echo $resultMessage;
